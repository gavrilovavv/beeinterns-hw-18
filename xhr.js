function login () {
    const xhr = new XMLHttpRequest();
    const url = 'https://reqres.in/api/login';

    xhr.open('POST', url, false);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');

    let data = JSON.stringify({
        'username': 'george.bluth@reqres.in',
        'email': 'george.bluth@reqres.in',
        'password': 'george',
    });

    xhr.send(data);
    let token = xhr.responseText;

    return (JSON.parse(token).token);
}

function getUsersEmail (token) {
    const xhr = new XMLHttpRequest();
    const url = 'https://reqres.in/api/users?per_page=10';

    xhr.onreadystatechange = function () {
        console.log(xhr.readyState);
        if (xhr.readyState === 4) {
            let emails = JSON.parse(xhr.responseText).data.map(elem => elem.email);
            console.log(emails);
        }
    }

    xhr.open('GET', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Accept-Language', 'ru-RU');
    xhr.setRequestHeader('Authorization', token);

    xhr.send();
}

getUsersEmail(login());